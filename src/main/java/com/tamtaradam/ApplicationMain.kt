package com.tamtaradam

import com.tamtaradam.views.MainController
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.layout.AnchorPane
import javafx.stage.Stage

/**
 * Created by Vadim Zalyubovskiy aka tamtaradam on 22.08.16.
 */

fun main(args: Array<String>) {
    ImageStego().runApp(args)
}

class ImageStego: Application() {

    fun runApp(args: Array<String>) {
        launch(*args)
    }

    override fun start(primaryStage: Stage?) {
        val loader = FXMLLoader()
        loader.location = ImageStego::class.java.getResource("/views/main_window.fxml")
        val mainWindow: AnchorPane = loader.load()
        val controller: MainController = loader.getController()
        controller.primaryStage = primaryStage as Stage
        val scene = Scene(mainWindow)
        primaryStage.scene = scene
        primaryStage.show()
    }
}