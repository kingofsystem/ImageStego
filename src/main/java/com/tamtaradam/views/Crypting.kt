package com.tamtaradam.views

import com.tamtaradam.*
import javafx.scene.image.Image
import javafx.scene.image.PixelReader
import javafx.scene.image.PixelWriter
import javafx.scene.image.WritableImage
import java.awt.Point
import java.util.*

/**
 * Created by Vadim Zalyubovskiy aka tamtaradam on 22.08.16.
 */

fun numberToCoord(num: Int, width: Int): Point = Point(num % width, num / width)

fun numberToCoord(num: Int, width: Double): Point = numberToCoord(num, width.toInt())

fun fillUnusedPixels(unusedPixels: ArrayList<Point>, width: Int, height: Int) {
    for (x in 0..width-1){
        for (y in 0..height-1) {
            unusedPixels.add(Point(x, y))
        }
    }
}

fun encryptData(image: Image, data: String, keyPixel: Int): Pair<Image?, Int> {
    val imageSize = (image.height * image.width).toInt()

    if (       (keyPixel < 0)
            or (keyPixel > image.width * image.height)
            or (data.toByteArray().size + 10 > image.height*image.width)
       ) {
        return Pair(null, 1)
    }

    val writableImage = WritableImage(image.pixelReader, image.width.toInt(), image.height.toInt())
    val pixelWriter = writableImage.pixelWriter
    val pixelReader = writableImage.pixelReader
    val dataBytes = data.toByteArray()

    val unUsedPixels = ArrayList<Point>()
    fillUnusedPixels(unUsedPixels, image.width.toInt(), image.height.toInt())

    val keyPixelPt = unUsedPixels[keyPixel-1]
    unUsedPixels.remove(keyPixelPt)
    val randomSeed = pixelReader.getArgb(keyPixelPt.x, keyPixelPt.y) and 0xffffff
    val random = Random(randomSeed.toLong() and 0xffffff)

    val getRandPixel = constructRandPixFunc(random, unUsedPixels)

    val messageLen = splitIntToBytes(dataBytes.size)
    writeDataToPixels(Pair(pixelReader, pixelWriter), getRandPixel, messageLen)
    writeDataToPixels(Pair(pixelReader, pixelWriter), getRandPixel, dataBytes)
    return Pair(writableImage, 0)
}

/*
Writing
 */
fun writeDataToPixels(pixelIO: Pair<PixelReader, PixelWriter>, getRandPixels: () -> Point, data: ByteArray) {
    data.forEach { b ->
        val pixels = Array(3, {getRandPixels()})
        writeByteToPixels(pixelIO, pixels, b)
    }
}

fun writeByteToPixels(pixelIO: Pair<PixelReader, PixelWriter>, pixels: Array<Point>, data: Byte) {
    val pixelReader = pixelIO.first
    val pixelWriter = pixelIO.second

    val newChannels = computeDataToChannels(pixelReader, pixels, data)
    pixels.forEachIndexed { i, point ->
        pixelWriter.writeBytesToRgb(point, newChannels[i], pixelReader.getArgb(point.x, point.y))
    }
}

fun computeDataToChannels(pixelReader: PixelReader, pixels: Array<Point>, data: Byte): Array<IntArray>  {
    val data = data.toInt()

    val rgbBytes = Array(pixels.size, { i -> pixelReader.readRgbToBytes(pixels[i]) })

    for (i in 0..2) {
        for (j in 0..2) {
            val dataBit = (data shr i*3+j) and 1
            if (dataBit == 1) {
                rgbBytes[i][j] = rgbBytes[i][j] or 1
            } else {
                rgbBytes[i][j] = rgbBytes[i][j] and 1.inv()
            }
            rgbBytes[i][j] = rgbBytes[i][j] and 0xff
        }
    }
    return rgbBytes
}


/*
Reading
 */
fun decryptData(image: Image, keyPixel: Int): String {
    val pixelReader = image.pixelReader
    val unUsedPixels = ArrayList<Point>()
    fillUnusedPixels(unUsedPixels, image.width.toInt(), image.height.toInt())

    val keyPixelPt = unUsedPixels[keyPixel-1]
    unUsedPixels.remove(keyPixelPt)

    val randomSeed = pixelReader.getArgb(keyPixelPt.x, keyPixelPt.y) and 0xffffff
    val random = Random(randomSeed.toLong() and 0xffffff)

    val getRandPixel = constructRandPixFunc(random, unUsedPixels)

    val messageLen = mergeBytesToInt(readDataFromPixels(pixelReader, 4, getRandPixel))
    return String(readDataFromPixels(pixelReader, messageLen, getRandPixel))
}

fun readDataFromPixels(pixelReader: PixelReader, dataLen: Int, getRandPixels: () -> Point): ByteArray {
    return ByteArray(dataLen, {i ->
        val pixels = Array(3, {getRandPixels()})
        readByteFromPixels(pixelReader, pixels)
    })
}

fun readByteFromPixels(pixelReader: PixelReader, pixels: Array<Point>): Byte {
    val rgbBytes = ArrayList<Int>()
    pixels.forEach { p -> rgbBytes.addAll(pixelReader.readRgbToBytes(p).asList()) }
    rgbBytes.removeAt(8)
    var data = 0
    for (i in 0..7) {
        data = (data or ((rgbBytes[i] and  1) shl i))
    }

    return data.toByte()
}