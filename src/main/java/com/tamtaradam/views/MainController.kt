package com.tamtaradam.views

import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.image.ImageView
import javafx.stage.FileChooser
import javafx.stage.Stage
import java.util.*

/**
 * Created by Vadim Zalyubovskiy aka tamtaradam on 22.08.16.
 */

class MainController {

    @FXML
    private lateinit var fileChooseBtn: Button

    @FXML
    private lateinit var imageName: Label

    @FXML
    private lateinit var preImage: ImageView

    @FXML
    private lateinit var postImage: ImageView

    @FXML
    private lateinit var encryptBtn: Button

    @FXML
    private lateinit var decryptBtn: Button

    @FXML
    private lateinit var imageDataText: TextArea

    @FXML
    private lateinit var keyPixelText: TextField

    lateinit var primaryStage: Stage

    @FXML
    private fun initialize() {
        fileChooseBtn.setOnMouseClicked {
            val fileChooser = FileChooser()
            fileChooser.title = "Choose image"
            println("14324")
            fileChooser.extensionFilters.addAll(
                    FileChooser.ExtensionFilter("jpg", "png", "jpeg")
            )
            val file = fileChooser.showOpenDialog(primaryStage)
            if (file != null) {
                println("14324")
            }
        }
    }
}