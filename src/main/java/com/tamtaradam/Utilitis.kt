package com.tamtaradam

import com.tamtaradam.views.numberToCoord
import javafx.scene.image.PixelReader
import javafx.scene.image.PixelWriter
import java.awt.Point
import java.io.InputStream
import java.net.URL
import java.util.*

/**
 * Created by Vadim Zalyubovskiy aka tamtaradam on 25.08.16.
 */

fun getInternalFileUrl(path: String): URL =
        Any::class.java.getResource(path)

fun getInternalFileStream(path: String): InputStream =
        Any::class.java.getResourceAsStream(path)

fun <T: PixelReader> T.readRgbToBytes(p: Point): IntArray {
    val argb = this.getArgb(p.x, p.y)
    return IntArray(3, { i ->
        return@IntArray (argb shr i * 8).toByte().toInt()
    }).reversedArray()
}

fun <T: PixelWriter> T.writeBytesToRgb(p: Point, rgbBytes: IntArray, oldColor: Int) {
    var rgbInt = 0
    rgbBytes.reversedArray().forEachIndexed { i, e -> rgbInt = rgbInt or (e shl i * 8) }
    this.setArgb(p.x, p.y, rgbInt or (oldColor and (0xff shl 24)))
}

fun splitIntToBytes(x: Int): ByteArray {
    return ByteArray(4, { i-> (x shr i*8).toByte() }).reversedArray()
}

fun mergeBytesToInt(bytes: ByteArray): Int {
    var x = 0
    bytes.reversedArray().forEachIndexed { i, byte -> x = x or ((byte.toInt() and 0xff) shl i * 8) }
    return x
}

fun constructRandPixFunc(random: Random, unUsedPixel: ArrayList<Point>): () -> Point {

    return fun (): Point {
        val index = random.nextInt(unUsedPixel.size)
        val pixel: Point = unUsedPixel.removeAt(index)
        return pixel
    }
}

