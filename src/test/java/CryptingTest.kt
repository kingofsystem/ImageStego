import com.tamtaradam.getInternalFileStream
import com.tamtaradam.readRgbToBytes
import com.tamtaradam.views.*
import javafx.embed.swing.SwingFXUtils
import javafx.scene.image.Image
import javafx.scene.image.PixelReader
import javafx.scene.image.WritableImage
import org.junit.Test
import java.awt.Point
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.util.*
import javax.imageio.ImageIO
import kotlin.test.assertEquals

/**
 * Created by Vadim Zalyubovskiy aka tamtaradam on 25.08.16.
 */

const val TARGET_DATA = 42.toByte()
const val CURRENT_SEED = 69.toLong()
class CryptingTest {

    private fun computeSum(pixelReader: PixelReader, pixels: Array<Point>): Int {
        var currentSum = 0
        pixels.forEach { p ->
            pixelReader.readRgbToBytes(p).forEach { b ->
                currentSum += b
            }
        }
        return currentSum
    }

    @Test
    fun byteEncryptionTest() {
        val testImage = Image(getInternalFileStream("/images/test.png"))
        val writableImage = WritableImage(testImage.pixelReader, testImage.width.toInt(), testImage.height.toInt())
        val random = Random()

        val pixels = Array(3, {
            numberToCoord(random.nextInt((testImage.height * testImage.width).toInt()), testImage.width)
        })
        writeByteToPixels(Pair(writableImage.pixelReader, writableImage.pixelWriter), pixels, TARGET_DATA)
        val readedData = readByteFromPixels(writableImage.pixelReader, pixels)
        assertEquals(TARGET_DATA, readedData)
    }

    @Test
    fun messageEncryptionTest() {
        val testImage = Image(getInternalFileStream("/images/image.jpg"))

        val testMessage = BufferedReader(InputStreamReader(getInternalFileStream("/test/test.txt"))).readText()

        val r = Random()
        val key = r.nextInt(testImage.width.toInt() * testImage.height.toInt())

        val encryptedImage = encryptData(testImage, testMessage, key)
        val decryptedMessage = decryptData(encryptedImage.first as Image, key)
        assertEquals(testMessage, decryptedMessage)
        println("Encrypted: $testMessage, decrypted: $decryptedMessage\n" +
                "Key was: $key")


        val bufImage = SwingFXUtils.fromFXImage(encryptedImage.first, null)
        ImageIO.write(bufImage, "png", File("encrypted.png"))
    }

}
